const quoteText = document.querySelector('.wrapper--quote');
const quoteAuthor = document.querySelector('.wrapper--author');
const quoteBtn = document.querySelector('.wrapper--generate');
const loader = document.querySelector('.circle-loading');
const wrapper = document.querySelector('.wrapper');

async function quoteGenerator() {
  loading();
  const proxyURL = 'https://cors-anywhere.herokuapp.com/';
  const apiURL =
    'https://api.forismatic.com/api/1.0/?method=getQuote&format=json&lang=en';

  try {
    const api_call = await fetch(proxyURL + apiURL);
    const data = await api_call.json();
    if (!data.quoteAuthor) {
      quoteAuthor.textContent = 'Somebody';
    } else {
      quoteAuthor.textContent = data.quoteAuthor;
    }
    quoteText.textContent = data.quoteText;

    complete();
  } catch (error) {
    console.log(error);
    quoteGenerator();
  }
}

quoteBtn.addEventListener('click', (_) => {
  quoteGenerator();
});

function loading() {
  wrapper.hidden = true;
  loader.hidden = false;
  loader.style.visibility = 'visible';
  wrapper.style.visibility = 'hidden';
}

function complete() {
  if (!loader.hidden) {
    wrapper.hidden = false;
    loader.hidden = true;
    wrapper.style.visibility = 'visible';
    loader.style.visibility = 'hidden';
  }
}
